#!/bin/sh

set -e

VLIST=$(dpkg -l | grep linux-image | awk '{print $2}' | sed -e 's:^linux-image-::g')
for V in $VLIST; do
echo Building AOE initramfs for $V
mkinitramfs -d /etc/initramfs-usb -o /tmp/initrd.img-usb-$V -v $V
done
