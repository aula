#!/bin/sh

set -e

#VLIST=$(dpkg -l | grep linux-image | awk '{print $2}' | sed -e 's:^linux-image-::g')
VLIST=2.6.32-5-686
for V in $VLIST; do
echo Building AOE initramfs for $V
mkinitramfs -d /etc/initramfs-auto -o /tmp/initrd.img-auto-$V -v $V
done

# cd /tmp
# cp initrd.img-aoe-2.6.32-5-686 initrd.img-aoe-coraid-2.6.32-5-686
# scp initrd.img-aoe-coraid-2.6.32-5-686 root@server:/srv/tftp/i386/
