#!/bin/bash

set -e
set -x

DSTDIR="/mnt/install_dest.tmp"

SCRIPTDIR="/usr/share/educastur/common-scripts/"

# If called with no arguments a new timer is returned.
# If called with arguments the first is used as a timer
# value and the elapsed time is returned in the form HH:MM:SS.
# See: http://www.linuxjournal.com/content/use-date-command-measure-elapsed-time
function timer()
{
    if [[ $# -eq 0 ]]; then
        echo $(date '+%s')
    else
        local  stime=$1
        etime=$(date '+%s')

        if [[ -z "$stime" ]]; then stime=$etime; fi

        dt=$((etime - stime))
        ds=$((dt % 60))
        dm=$(((dt / 60) % 60))
        dh=$((dt / 3600))
        printf '%d:%02d:%02d' $dh $dm $ds
    fi
}

t=$(timer)

umount "${DSTDIR}/dev/pts/" || umount -l "${DSTDIR}/dev/pts/" || true
umount "${DSTDIR}/dev/" || umount -l "${DSTDIR}/dev/" || true
umount "${DSTDIR}/sys" || umount -l "${DSTDIR}/sys" || true
umount "${DSTDIR}/proc" || umount -l "${DSTDIR}/proc" || true
umount "${DSTDIR}" || umount -l "${DSTDIR}" || true

rmdir "${DSTDIR}"  || true

if [ -e "${DSTDIR}" ]; then exit 1; fi

# Partition Exists
PART=$("${SCRIPTDIR}/partition-by-label" "AULA-LOCAL-WS")
if [ -z "$PART" ]; then echo "Destination partition not found"; exit 1; fi

DISK=$(echo "$PART" | grep -e "^[sh]d[a-z][0-9]*$" | sed 's/^\(...\).*$/\1/')
if [ -z "$DISK" ]; then echo "Disk type not supported"; exit 1; fi
echo "Disk: $DISK";

# Get Size
DEST_SIZE=$(cat /proc/partitions | grep -e "${PART}$" | awk '{print $3}')
if [ -z "$DEST_SIZE" ]; then "Destination partition size is not right"; exit 1; fi
if [ ${DEST_SIZE} -le 0 ]; then "Destination partition size is not right"; exit 1; fi

# Check that it is Not Mounted

# Ask for Confirmation
echo "Destination: $PART ($DEST_SIZE bytes)"

# Format
mkfs.ext3 -L "AULA-LOCAL-WS" "/dev/$PART"

mkdir -p "${DSTDIR}"

if [ ! -d "${DSTDIR}" ]; then exit 1; fi

mount "/dev/$PART" "${DSTDIR}"

rm -rf "${DSTDIR}/lost+found"

# Check Size
#SRC_SIZE=$(cat /proc/partitions | grep -e "etherd/${SRC_PART}$" | awk '{print $3}')
SRC_SIZE=10000000
if [ -z "$SRC_SIZE" ]; then exit 1; fi
if [ ${DEST_SIZE} -le ${SRC_SIZE} ]; then echo "Destination partition is too small"; exit 1; fi

echo "Source: rsync://server/ws"

rsync -avz "rsync://server/ws/*" "${DSTDIR}"

sleep 1

mount none -t proc "${DSTDIR}/proc/"
mount none -t sysfs "${DSTDIR}/sys"
mount --bind /dev "${DSTDIR}/dev/"
mount none -t devpts "${DSTDIR}/dev/pts/"

if [ ! -z "$DISK" ]; then
	echo "Installing GRUB in disk: $DISK";
	chroot "${DSTDIR}" grub-install --force "/dev/$DISK"
fi

chroot "${DSTDIR}" update-grub

# Umount destination
umount "${DSTDIR}/dev/pts/" || umount -l "${DSTDIR}/dev/pts/" 
umount "${DSTDIR}/dev/" || umount -l "${DSTDIR}/dev/"
umount "${DSTDIR}/sys" || umount -l "${DSTDIR}/sys"
umount "${DSTDIR}/proc" || umount -l "${DSTDIR}/proc"
umount "${DSTDIR}" || umount -l "${DSTDIR}"
rmdir "${DSTDIR}"

printf 'Elapsed time: %s\n' $(timer $t)

