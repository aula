#!/usr/bin/python 

# Send UDP broadcast packets

PORT = 31337

import sys, time
import select, socket

READ_ONLY = select.POLLIN | select.POLLPRI | select.POLLHUP | select.POLLERR
READ_WRITE = READ_ONLY | select.POLLOUT

s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
s.bind(('', 0))
s.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)

data = sys.stdin.read()
sys.stderr.write( "Sending message:\n%s\n" % (data) )

for i in range(10):
	# Broadcast announce
	sys.stderr.write( "Sending broadcast\n")
	s.sendto(data, ('<broadcast>', PORT))

	# Receive data back
	s.setblocking(0)
	data = ""

	#ready = select.select([s], [], [], TIMEOUT_IN_SECONDS)
	#if ready[0]:
	#	data = s.recv(8192)

	poll=select.poll()
	poll.register(s, READ_ONLY)

	poll.poll(50) # Timeout in milliseconds
	try:
		data = s.recv(8192)
	except:
		pass

	if data:
		print data
		sys.exit(0)

sys.exit(-1)
