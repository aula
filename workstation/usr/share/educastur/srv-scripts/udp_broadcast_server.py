#!/usr/bin/python

import socket, traceback
import os, sys, threading, time

HOST = '' # Bind to all interfaces
PORT = 31337

CHECK_PERIOD = 5
CHECK_TIMEOUT = 2
UNIX_SOCKET = "/var/run/aula-hb.socket"

class Heartbeats(dict):
    """Manage shared heartbeats dictionary with thread locking"""

    def __init__(self):
        super(Heartbeats, self).__init__()
        self._lock = threading.Lock()

    def __setitem__(self, key, value):
        """Create or update the dictionary entry for a client"""
        self._lock.acquire()
        if not super(Heartbeats, self).has_key(key):
            sys.stderr.write("New Client: " + key + "\n")
        super(Heartbeats, self).__setitem__(key, value)
        self._lock.release()

    def removeOld(self, timegap):
        if timegap is not None:
            limit = time.time() - float(timegap)
        else:
            limit = 0.0
        self._lock.acquire()
        for (ip, ipData) in self.items():
            if ipData[0] <= limit:
                sys.stderr.write("Client Removed: %s (timestamp = %.3f <= %.3f)\n" % (ip, ipData[0], limit))
                super(Heartbeats, self).__delitem__(ip)
        self._lock.release()

    def getRecent(self, timegap=None):
        """Return a list of clients with heartbeat newer than timegap"""
        if timegap is not None:
            limit = time.time() - float(timegap)
        else:
            limit = 0.0
        self._lock.acquire()
        items = [ip for (ip, ipData) in self.items() if ipData[0] > limit]
        self._lock.release()
        return items

    def getRecentString(self, timegap=None):
        """Return a string of clients with heartbeat newer than timegap"""
        current = time.time()
        if timegap is not None:
            limit = current - float(timegap)
        else:
            limit = 0.0
        self._lock.acquire()        
        str = ["%s\t%.3f\t%s" % (ip, current - ipData[0], ipData[1]) for (ip, ipData) in self.items() if ipData[0] > limit]
        self._lock.release()
        return "\n".join(str)

class Receiver(threading.Thread):
    """Receive UDP packets, process them and log them in the heartbeats dictionary"""
    def __init__(self, goOnEvent, heartbeats, server_data):
        super(Receiver, self).__init__()
        self.goOnEvent = goOnEvent
        self.heartbeats = heartbeats
        self.server_data = server_data

        self.socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
        self.socket.settimeout(CHECK_TIMEOUT)
        self.socket.bind((HOST, PORT))

    def run(self):
        sys.stderr.write("Starting Listening thread.\n")
        while self.goOnEvent.isSet():
            try:
                message, address = self.socket.recvfrom(8192)
                sys.stderr.write("Broadcast Server: Got data from %s\n" % (str(address)) )
                message=message.split("\n")
                mac=""
                query=""
                for line in message:
                    pieces=line.split("=")
                    if len(pieces)>=2:
                        key=pieces[0].strip()
                        value=pieces[1].strip()
                        print "%s -> %s" % (key, value)
                        if key.upper() =="MAC":
                            mac = value
                        elif key.upper()=="QUERY":
                            query = value
                self.heartbeats[address[0]] = (time.time(), mac)
                if query.upper() == "SERVER":
                    self.socket.sendto(self.server_data, address)
                else:
                    self.socket.sendto("ANSWER=ACK", address)
            except (KeyboardInterrupt, SystemExit):
                raise
            except socket.timeout:
                sys.stderr.write("*")
                self.heartbeats.removeOld(timegap=10)
                pass            
            except:
                traceback.print_exc()

class Control(threading.Thread):
    """Receive UNIX Socket control messages and reply to them"""
    def __init__(self, goOnEvent, heartbeats):
        super(Control, self).__init__()
        self.goOnEvent = goOnEvent
        self.heartbeats = heartbeats

        self.socket = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
        self.socket.settimeout(CHECK_TIMEOUT)
        
        try:
            os.remove(UNIX_SOCKET)
        except OSError:
            pass
        self.socket.bind(UNIX_SOCKET)
        os.chmod(UNIX_SOCKET,0777)

    def run(self):
        sys.stderr.write("Starting Control thread.\n")
        while self.goOnEvent.isSet():
            try:    
                self.socket.listen(1)
                conn, addr = self.socket.accept()
                while True:
                    query = conn.recv(1024)
                    if not query: break
                    answer = self.heartbeats.getRecentString(timegap=30)
                    answer += "\n"
                    conn.send(answer)
                conn.close()            
            except (KeyboardInterrupt, SystemExit):
                raise
            except socket.timeout:
                sys.stderr.write("+")
                pass            

def main():
    message = sys.stdin.read()
    sys.stderr.write( "Broadcast message:\n%s\n" % (message) )

    heartbeats = Heartbeats()

    controlEvent = threading.Event()
    controlEvent.set()
    control = Control(goOnEvent = controlEvent, heartbeats = heartbeats)
    control.start()

    receiverEvent = threading.Event()
    receiverEvent.set()
    receiver = Receiver(goOnEvent = receiverEvent, heartbeats = heartbeats, server_data = message)
    receiver.start()

    print ('Threaded heartbeat server listening on port %d\n'
        'press Ctrl-C to stop\n') % PORT
    try:
        while True:
            recent = heartbeats.getRecent()
            print 'Recent clients: %s' % recent
            time.sleep(CHECK_PERIOD)
    except KeyboardInterrupt:
        print 'Exiting, please wait...'
        receiverEvent.clear()
        receiver.join()
        controlEvent.clear()
        control.join()
        print 'Finished.'

if __name__ == '__main__':
    main()
