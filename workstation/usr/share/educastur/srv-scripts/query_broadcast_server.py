#!/usr/bin/python

import sys, socket

args = "\n".join(sys.argv[1:])
if not args: args = ""
args += "\n"

s = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
s.connect("/var/run/aula-hb.socket")
s.send(args)
data = s.recv(1024)
s.close()
sys.stderr.write("Received answer:\n")
print data
