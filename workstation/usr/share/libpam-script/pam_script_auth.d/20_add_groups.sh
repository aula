#!/bin/sh

# When set, the shell exits when a simple command in a command list exits
# non-zero (FALSE). This is not done in situations, where the exit code is
# already checked (if, while, until, ||, &&)
set -e

# Print commands just before execution - with all expansions and substitutions
# done, and words marked - useful for debugging.
#set -x

test -n "${PAM_USER}" || exit 0
test "${PAM_USER}" != "root" || exit 0
test "${PAM_USER_UID}" -ge 990 -o "${PAM_USER_GID}" -ge 990 || exit 0
test "${PAM_USER_UID}" -lt 60000 || exit 0
#test -d ${PAM_USER_HOME} || exit 0

. /etc/default/educastur_parameters
LDAP_ANS=$(ldapsearch -v -h server -x cn=* -b ou=PRO,${LDAP_BASE} objectclass=dn 2>/dev/null | grep -e "^dn: cn=${PAM_USER}" | sed "s/dn: cn=\([^,]*\).*$/\1/g")

echo ${LDAP_ANS}

TEACHER=false;
if [ -n "${LDAP_ANS}" ]; then
	TEACHER=true;
fi

usermod -a -G cdrom,floppy,audio,dip,video "${PAM_USER}"
if ${TEACHER}; then
	usermod -a -G teacher "${PAM_USER}"
fi

exit 0
