#!/bin/bash

# When set, the shell exits when a simple command in a command list exits
# non-zero (FALSE). This is not done in situations, where the exit code is
# already checked (if, while, until, ||, &&)
set -e

# Print commands just before execution - with all expansions and substitutions
# done, and words marked - useful for debugging.
#set -x

if [ ! -d "${PAM_USER_HOME}" ]; then
   mkdir -p "${PAM_USER_HOME}" 2>/dev/null
   #find /home/skel -name ".*" -exec cp -r \{\} ${PAM_USER_HOME} \;
   rmdir "${PAM_USER_HOME}" 2>/dev/null
   cp -r "/home/skel/" "${PAM_USER_HOME}"
   chown -R ${PAM_USER_UID}.${PAM_USER_GID} "${PAM_USER_HOME}" 2>/dev/null
   if [ ! -z "${PAM_USER_HOME}" ]; then
      rm -rf "${PAM_USER_HOME}"/.gnome2/keyrings/*;
   fi
   chmod 700 "${PAM_USER_HOME}" 2>/dev/null
fi

exit 0
