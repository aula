#!/bin/sh

# When set, the shell exits when a simple command in a command list exits
# non-zero (FALSE). This is not done in situations, where the exit code is
# already checked (if, while, until, ||, &&)
set -e

# Print commands just before execution - with all expansions and substitutions
# done, and words marked - useful for debugging.
#set -x

test -n "${PAM_USER}" || exit 0
test "${PAM_USER}" != "root" || exit 0
test "${PAM_USER}" != "test" || exit 0
test "${PAM_USER}" != "guest" || exit 0
test "${PAM_USER}" != "skel" || exit 0
test "${PAM_USER_UID}" -ge 990 -o "${PAM_USER_GID}" -ge 990 || exit 0
test "${PAM_USER_UID}" -lt 60000 || exit 0
test -d ${PAM_USER_HOME} || exit 0
BKMARKS=${PAM_USER_HOME}/.gtk-bookmarks
test -f ${BKMARKS} || exit 0

cp -fa ${BKMARKS} ${BKMARKS}~
cat ${BKMARKS}~ \
	| sed \
		-e "s:file\:///var/home/guest:file\://${PAM_USER_HOME}:g" \
		-e "s:file\:///home/skel:file\://${PAM_USER_HOME}:g" \
	> ${BKMARKS}
rm -f ${BKMARKS}~

exit 0
