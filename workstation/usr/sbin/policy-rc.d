#!/bin/sh
## Don't start any service if running in a chroot.
## See: http://unix.stackexchange.com/questions/12956/how-do-i-run-32-bit-programs-on-a-64-bit-ubuntu
## See: /usr/share/doc/sysv-rc/README.policy-rc.d.gz
if [ "$(stat -c %d:%i /)" != "$(stat -c %d:%i /proc/1/root/.)" ]; then
  exit 101
fi
