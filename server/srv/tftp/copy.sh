#!/bin/sh

set -e
set -x

cp -v /usr/lib/syslinux/gpxelinux.0 i386/
cp -v /usr/lib/syslinux/menu.c32 i386/
cp -v /usr/lib/syslinux/vesamenu.c32 i386/
cp -v /usr/lib/syslinux/chain.c32 i386/
mkdir -p i386/pxelinux.cfg
