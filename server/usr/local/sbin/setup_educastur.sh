#! /bin/sh

# Author: Miriam Ruiz <miriam@debian.org>

# Read configuration variable file if it is present
[ -r /etc/default/educastur_parameters ] && . /etc/default/educastur_parameters

/usr/local/sbin/clean_teachers

TIMESTAMP=$(date -u '+%Y%m%d_%H%M')
find /etc/templates/ -type f | sed -e "s|^/etc/templates/||" | while read FILE; do
	BASENAME=$(basename "/etc/$FILE")
	DIRNAME=$(dirname "/etc/$FILE")
	HASHFILE="/var/lib/educastur/sha1$DIRNAME/$BASENAME"
	mkdir -p "$DIRNAME"
	if [ -f "$DIRNAME/$BASENAME" ]; then
		SHA1SUM=$(sha1sum "$DIRNAME/$BASENAME" | awk '{ print $1}')
		if [ ! -f "$HASHFILE" ] || [ "$(cat "$HASHFILE")" != "$SHA1SUM" ]; then
			echo "Renaming $DIRNAME/$BASENAME to $DIRNAME/$BASENAME.BCK_$TIMESTAMP"
			mv "$DIRNAME/$BASENAME" "$DIRNAME/$BASENAME.BCK_$TIMESTAMP"
		fi
	fi
	echo "Using /etc/templates/$FILE to generate $DIRNAME/$BASENAME"
	cat "/etc/templates/$FILE" \
		| sed -e "s|[$]SERVER_LAN[$]|${SERVER_LAN}|g" \
		| sed -e "s|[$]SERVER_IP[$]|${SERVER_IP}|g" \
		| sed -e "s|[$]NETWORK_ADDRESS[$]|${NETWORK_ADDRESS}|g" \
		| sed -e "s|[$]NETWORK_MASK[$]|${NETWORK_MASK}|g" \
		| sed -e "s|[$]NETWORK_MASK_SIZE[$]|${NETWORK_MASK_SIZE}|g" \
		| sed -e "s|[$]NETWORK_BCAST[$]|${NETWORK_BCAST}|g" \
		| sed -e "s|[$]NETWORK_GATE[$]|${NETWORK_GATE}|g" \
		| sed -e "s|[$]NAMESERVER1[$]|${NAMESERVER1}|g" \
		| sed -e "s|[$]NAMESERVER2[$]|${NAMESERVER2}|g" \
		| sed -e "s|[$]WEBPROXY_HOST[$]|${WEBPROXY_HOST}|g" \
		| sed -e "s|[$]WEBPROXY_PORT[$]|${WEBPROXY_PORT}|g" \
		| sed -e "s|[$]DHCPRANGE_MIN[$]|${DHCPRANGE_MIN}|g" \
		| sed -e "s|[$]DHCPRANGE_MAX[$]|${DHCPRANGE_MAX}|g" \
		| sed -e "s|[$]DHCPRANGE_NET[$]|${DHCPRANGE_NET}|g" \
		| sed -e "s|[$]DHCPRANGE_MASK[$]|${DHCPRANGE_MASK}|g" \
		| sed -e "s|[$]LDAP_URI[$]|${LDAP_URI}|g" \
		| sed -e "s|[$]LDAP_BASE[$]|${LDAP_BASE}|g" \
		| sed -e "s|[$]LDAP_ROOTBIND[$]|${LDAP_ROOTBIND}|g" \
			> "$DIRNAME/$BASENAME"
	mkdir -p "/var/lib/educastur/sha1$DIRNAME"
	echo "$SHA1SUM" > "$HASHFILE"
done

/usr/local/sbin/adj_teachers
