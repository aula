#
# See slapd.conf(5) for details on configuration options.
# This file should NOT be world readable.
#

# =( Red Educativa del Principado de Asturias )==========================
#  If you need to modify this file, change it at /etc/templates/
#  Otherwise your changes will be overwritten when the system is started
# =======================================================================

include /etc/ldap/schema/core.schema
include /etc/ldap/schema/cosine.schema
include /etc/ldap/schema/inetorgperson.schema
include /etc/ldap/schema/nis.schema
include /etc/ldap/schema/misc.schema
include /etc/ldap/schema/openldap.schema
include /etc/ldap/schema/ppolicy.schema

# Allow LDAPv2 client connections. This is NOT the default.
#allow bind_v2

# Do not enable referrals until AFTER you have a working directory
# service AND an understanding of referrals.
#referral ldap://root.openldap.org

pidfile /var/run/slapd/slapd.pid
argsfile /var/run/slapd/slapd.args

# Load dynamic modules:
# http://www.symas.com/blog/?page_id=76

#moduleload      back_monitor
moduleload      back_bdb
#moduleload      back_dnssrv
#moduleload      back_hdb
#moduleload      back_ldbm
#moduleload      back_meta
#moduleload      back_null
#moduleload      back_passwd
#moduleload      back_perl
#moduleload      back_shell
#moduleload      back_sql
moduleload      back_ldap

# moduleload accesslog.la
# moduleload auditlog.la
# moduleload back_sql.la
# moduleload denyop.la
# moduleload dyngroup.la
# moduleload dynlist.la
# moduleload lastmod.la
moduleload pcache.la
# moduleload ppolicy.la
# moduleload refint.la
# moduleload retcode.la
 moduleload rwm.la
# moduleload syncprov.la
# moduleload translucent.la
# moduleload unique.la
# moduleload valsort.la

moduleload rwm

# The next three lines allow use of TLS for encrypting connections using a
# dummy test certificate which you can generate by changing to
# /etc/pki/tls/certs, running "make slapd.pem", and fixing permissions on
# slapd.pem so that the ldap user or group can read it. Your client software
# may balk at self-signed certificates, however.
# TLSCACertificateFile /etc/pki/tls/certs/ca-bundle.crt
# TLSCertificateFile /etc/pki/tls/certs/slapd.pem
# TLSCertificateKeyFile /etc/pki/tls/certs/slapd.pem

# Sample security restrictions
# Require integrity protection (prevent hijacking)
# Require 112-bit (3DES or better) encryption for updates
# Require 63-bit encryption for simple bind
# security ssf=1 update_ssf=112 simple_bind=64

# Sample access control policy:
# Root DSE: allow anyone to read it
# Subschema (sub)entry DSE: allow anyone to read it
# Other DSEs:
# Allow self write access
# Allow authenticated users read access
# Allow anonymous users to authenticate
# Directives needed to implement policy:
# access to dn.base="" by * read
# access to dn.base="cn=Subschema" by * read
# access to *
# by self write
# by users read
# by anonymous auth
#
# if no access controls are present, the default policy
# allows anyone and everyone to read anything but restricts
# updates to rootdn. (e.g., "access to * by * read")
#
# rootdn can always read and write EVERYTHING!

################################################## #####################
# ldbm and/or bdb database definitions
################################################## #####################

database bdb

suffix dc=local
rootdn cn=admin,dc=local
# Cleartext passwords, especially for the rootdn, should
# be avoided. See slappasswd(8) and slapd.conf(5) for details.
# Use of strong authentication encouraged.
# rootpw secret
rootpw {SSHA}kWOEK9jqjgQPryDSQlPM33796VgS9qOb

# The database directory MUST exist prior to running slapd AND
# should only be accessible by the slapd and slap tools.
# Mode 700 recommended.
directory /var/lib/ldap

# Indices to maintain for this database
index           objectClass eq
index           uid         eq

# Replicas of this database
#replogfile /var/lib/ldap/openldap-master-replog
#replica host=ldap-1.example.com:389 starttls=critical
# bindmethod=sasl saslmech=GSSAPI
# authcId=host/ldap-master.example.com@EXAMPLE.COM

database ldap

# http://ubuntuforums.org/showthread.php?p=9899058	
# http://www.clearfoundation.com/component/option,com_kunena/Itemid,232/catid,10/func,view/id,18968/

uri "ldap://192.168.14.4"
suffix "ou=AV12,ou=D5,ou=D,o=EPA"
rootdn "cn=admin,ou=AV12,ou=D5,ou=D,o=EPA"
rootpw {SSHA}kWOEK9jqjgQPryDSQlPM33796VgS9qOb

#tls start
#idassert-bind bindmethod=simple binddn="cn=admin,dc=example,dc=net" credentials="example" mode=none
#idassert-authzFrom "dn.subtree:dc=example,dc=com"

# http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=562723

overlay chain
chain-cache-uri true

overlay rwm
rwm-rewriteEngine on
rwm-normalize-mapped-attrs yes
rwm-drop-unrequested-attrs yes

rwm-map objectclass posixaccount *
rwm-map objectclass posixgroup *
rwm-map objectclass *

rwm-map attribute cn *
rwm-map attribute sn *
rwm-map attribute givenName *
rwm-map attribute mail *
rwm-map attribute uid *
rwm-map attribute uidNumber *
rwm-map attribute gidNumber *
rwm-map attribute homeDirectory *
rwm-map attribute userPassword *
rwm-map attribute loginShell *
rwm-map attribute gecos *
rwm-map attribute description *
#rwm-map attribute groupMembership *
rwm-map attribute member *
rwm-map attribute *

overlay pcache
proxycache bdb 10000 1 50 100
directory /var/lib/ldap/cache
cachesize 1000000

index uid eq
index mail eq
index uidNumber eq
index gidNumber eq
index memberUid eq
index description eq
index sn eq
index cn pres,eq,sub

proxycachequeries 10000
proxyattrset 0 uid mail cn sn givenName objectClass
proxytemplate (uid=) 0 600
proxytemplate (cn=) 0 600
proxytemplate (objectclass=) 0 600
proxytemplate (mail=) 0 600
proxytemplate (&(uid=)(mail=)) 0 600
proxytemplate (&(uid=)(objectclass=)) 0 600
proxytemplate (&(objectclass=)(cn)) 0 600
proxytemplate (&(uid=)(objectclass=)(cn)) 0 600 
